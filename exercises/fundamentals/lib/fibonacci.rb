# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num <= 0
  return num - 1 if [1, 2].include?(num)

  fib_array = [0, 1]
  (2..num - 1).each { fib_array << fib_array[-1] + fib_array[-2] }
  fib_array[num - 1]
end
# END
