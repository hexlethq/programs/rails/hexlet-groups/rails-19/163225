# frozen_string_literal: true

# BEGIN
def reverse(string)
  split_string = string.chars
  reverse_string = []
  string.length.times { reverse_string << split_string.pop }
  reverse_string.join
end
# END
